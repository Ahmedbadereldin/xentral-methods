package com.xentralmethods.xentralmethods.interfaces;

public interface ItemInterface {
    boolean isSection();
}