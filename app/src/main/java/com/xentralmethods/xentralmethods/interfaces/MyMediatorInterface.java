package com.xentralmethods.xentralmethods.interfaces;


import com.xentralmethods.xentralmethods.models.BookModel;

public interface MyMediatorInterface {
    void userItemClick(BookModel model);
}