package com.xentralmethods.xentralmethods;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.xentralmethods.xentralmethods.adapter.MyAdapter;
import com.xentralmethods.xentralmethods.interfaces.ItemInterface;
import com.xentralmethods.xentralmethods.interfaces.MyMediatorInterface;
import com.xentralmethods.xentralmethods.models.BookModel;
import com.xentralmethods.xentralmethods.models.HeaderModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MyMediatorInterface {
    private static final String TAG = "MainActivity";

    private ArrayList<BookModel> bookModels;
    private MyAdapter mAdapter;
    private ArrayList<ItemInterface> mItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpView();
    }

    private void setUpView() {
        RecyclerView mRecyclerView = findViewById(R.id.rv);
        bookModels = new ArrayList<>();
        addItemsFromJSON();

        mItems = new ArrayList<>();
        getSectionalList(bookModels);

        mRecyclerView.setHasFixedSize(true);

        final GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {

            @Override
            public int getSpanSize(int position) {
                if (MyAdapter.SECTION_VIEW == mAdapter.getItemViewType(position)) {
                    return 2;
                }
                return 1;
            }
        });
        mRecyclerView.setLayoutManager(gridLayoutManager);

        mAdapter = new MyAdapter(mItems, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void getSectionalList(ArrayList<BookModel> bookModels) {
        int size = bookModels.size();
        int count = 0;

        for (int i = 0; i < size; i++) {
            count++;
            mItems.add(bookModels.get(i));
            if (count % 2 == 0) {
                mItems.add(new HeaderModel());
            }
        }
    }

    private void addItemsFromJSON() {
        try {

            String jsonDataString = readJSONDataFromFile();
            JSONArray jsonArray = new JSONArray(jsonDataString);

            for (int i = 0; i < jsonArray.length(); ++i) {

                JSONObject itemObj = jsonArray.getJSONObject(i);

                String title = itemObj.getString("title");
                String cover = itemObj.getString("cover");
                int img;

                switch (cover) {
                    case "cover2.png":
                        img = R.drawable.cover2;
                        break;
                    case "cover3.png":
                        img = R.drawable.cover3;
                        break;
                    case "cover4.png":
                        img = R.drawable.cover4;
                        break;
                    default:
                        img = R.drawable.cover1;
                }

                BookModel model = new BookModel(title, img);
                bookModels.add(model);
            }

        } catch (JSONException | IOException e) {
            Log.d(TAG, "addItemsFromJSON: ", e);
        }
    }

    private String readJSONDataFromFile() throws IOException {

        InputStream inputStream = null;
        StringBuilder builder = new StringBuilder();

        try {

            String jsonString = null;
            inputStream = getResources().openRawResource(R.raw.data);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, "UTF-8"));

            while ((jsonString = bufferedReader.readLine()) != null) {
                builder.append(jsonString);
            }

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return new String(builder);
    }

    @Override
    public void userItemClick(BookModel model) {
        showBookDetails(model);
    }

    private void showBookDetails(BookModel bookModel) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        View view = getLayoutInflater().inflate(R.layout.dialog_show_book, null);
        ImageView ivItem = view.findViewById(R.id.ivItem);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        ivItem.setImageResource(bookModel.getCover());
        tvTitle.setText(bookModel.getTitle());

        dialog.setContentView(view);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }
}