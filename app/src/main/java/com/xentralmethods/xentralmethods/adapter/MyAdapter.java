package com.xentralmethods.xentralmethods.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xentralmethods.xentralmethods.MainActivity;
import com.xentralmethods.xentralmethods.R;
import com.xentralmethods.xentralmethods.interfaces.ItemInterface;
import com.xentralmethods.xentralmethods.models.BookModel;
import com.xentralmethods.xentralmethods.models.HeaderModel;

import java.lang.ref.WeakReference;
import java.util.ArrayList;


public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int SECTION_VIEW = 0;
    public static final int CONTENT_VIEW = 1;

    ArrayList<ItemInterface> mItems;
    WeakReference<Context> mContextWeakReference;

    public MyAdapter(ArrayList<ItemInterface> items, Context context) {
        this.mItems = items;
        this.mContextWeakReference = new WeakReference<>(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Context context = mContextWeakReference.get();
        if (viewType == SECTION_VIEW) {
            return new SectionViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.header_books, parent, false));
        }
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_books, parent, false), context);
    }

    @Override
    public int getItemViewType(int position) {
        if (mItems.get(position).isSection()) {
            return SECTION_VIEW;
        } else {
            return CONTENT_VIEW;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {

        Context context = mContextWeakReference.get();

        if (context == null) {
            return;
        }

        if (SECTION_VIEW == getItemViewType(position)) {
            SectionViewHolder sectionViewHolder = (SectionViewHolder) holder;
            HeaderModel headerModel = ((HeaderModel) mItems.get(position));
//            sectionViewHolder.ivItem.setImageResource(bookModel.getCover());
            return;
        }

        MyViewHolder myViewHolder = (MyViewHolder) holder;
        BookModel bookModel = ((BookModel) mItems.get(position));

        myViewHolder.ivItem.setImageResource(bookModel.getCover());
        myViewHolder.ivItem.setOnClickListener(view -> ((MainActivity) context).userItemClick(bookModel));

    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivItem;
        public MyViewHolder(View itemView, final Context context) {
            super(itemView);
            ivItem = itemView.findViewById(R.id.ivItem);
        }
    }

    public static class SectionViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivItem;

        public SectionViewHolder(View itemView) {
            super(itemView);
            ivItem = itemView.findViewById(R.id.ivItem);
        }
    }
}