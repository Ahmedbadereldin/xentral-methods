
package com.xentralmethods.xentralmethods.models;


import com.xentralmethods.xentralmethods.interfaces.ItemInterface;

public class HeaderModel implements ItemInterface {
    private String cover;

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    @Override
    public boolean isSection() {
        return true;
    }
}
