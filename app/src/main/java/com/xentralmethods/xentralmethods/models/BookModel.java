
package com.xentralmethods.xentralmethods.models;


import com.xentralmethods.xentralmethods.interfaces.ItemInterface;

public class BookModel implements ItemInterface {
    private int cover;
    private String title;

    public BookModel(String title, int cover) {
        this.cover = cover;
        this.title = title;
    }

    public int getCover() {
        return cover;
    }

    public void setCover(int cover) {
        this.cover = cover;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean isSection() {
        return false;
    }
}
